package Ejercicio4;

import java.io.*;
import java.util.HashMap;
import java.util.Scanner;

public class Traductor {
    private static HashMap<String, String> hm;
    //archivo para las traducciones
    private static String archivo = "diccionario.txt";

    public static void main(String[] args) {
        hm = new HashMap<>();
        lista(args);
        //leemos la entrada
        Scanner sc = new Scanner(System.in);
        String linea;
        linea = sc.nextLine();

        while (!linea.toLowerCase().equals("0")) {
            //imprimimos la traducción de la palabra y si no te dirá que no conoce la palabra
            if (hm.containsKey(linea)) {
                System.out.println(hm.get(linea));
            } else {
                System.out.println("Desconocido");
            }
            linea = sc.nextLine();
        }
        sc.close();
        }

        public static void lista(String[] lista){
            //si la primera posición no está vacio y le asigna ese valor al diccionario
            if (lista.length > 0) {
                archivo = lista[0];
            }
            try{
                File f = new File(archivo);
                BufferedReader br = new BufferedReader(new FileReader(f));
                String linea = br.readLine();
                //pasa cada línea a un método para separar la palabra original y la traduccion por el separador
                while(linea != null) {
                    separar(linea);
                    linea = br.readLine();
                }
                br.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    //separamos la palabra en ingles con la traducción a través de un ';'
    public static void separar(String linea){
        String[] traduccion = linea.split(";");
        hm.put(traduccion[0], traduccion[1]);
    }
}
