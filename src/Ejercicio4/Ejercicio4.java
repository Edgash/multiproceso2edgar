package Ejercicio4;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


public class Ejercicio4 {
    private static List<String> traduccion;
    private static final String d = "-d";
    private static final String f = "-f";
    private static String diccionario;
    private static String fichero;

    public static void main(String[] args) {
        //iniciamos la lista de palabras que creadas
        traduccion = new ArrayList<>();
        //creamos el comando para la ejecución del hijo
        String command = "java -jar out/Ejercicio4_jar/MultiprocesoEdgar2.jar";
        args(args);
        //covertimos el comando en una lista
        List<String> lista = new ArrayList<>(Arrays.asList(command.split(" ")));
        //si el comando está en null no hacemos nada, pero si no le añadimos un valor
        if (diccionario == null) {
        }else{
            traduccion.add(diccionario);
        }

        //asegura captar errores si estos salen durante
        try {
            ProcessBuilder pb = new ProcessBuilder(lista);
            Process process = pb.start();
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));

            Scanner palabra = new Scanner(process.getInputStream());
            //si has introducido un diccionario, usará este
            if (fichero != null) {
                //lee el fichero
                BufferedReader br = new BufferedReader(new FileReader(fichero));
                //se le asigna una linea a una variable
                String original = br.readLine();
                String trad;
                //mientras haya información en el fichero seguirá añadiendo palabras al hijo
                while (original != null) {
                    bw.write(original);
                    bw.newLine();
                    bw.flush();
                    trad = palabra.nextLine();
                    //llamamos al método 'añadir'
                    añadir(original, trad);
                    //leemos la siguiente linea del fichero
                    original = br.readLine();
                }
                br.close();
            }
            //llamamos al método 'guardar'
            guardar();
        //pilla los posibles errores que se pueden dar
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
    //este método llama a otros 2 métodos
    public static void args(String[] args){
        List<String> lista = Arrays.asList(args);

        ifDiccionario(lista);
        ifFichero(lista);
    }

    public static void ifDiccionario(List<String> lista){
        //comprobamos si el argumento contiene '-d'
        if(lista.contains(d)){
            if (lista.indexOf(d) < lista.size() - 1) {
                //asignamos el valor a la lista del diccionario
                diccionario = lista.get(lista.indexOf(d) + 1);
            }else{
                System.out.println("Diccionario desconocido, se le asignará el diccionario interno.");
            }
        }else{
            System.out.println("Diccionario desconocido, se le asignará el diccionario interno.");
        }
    }

    public static void ifFichero(List<String> lista){
        //comprobamos si el argumento contiiene '-f'
        if(lista.contains(f)){
            if (lista.indexOf(f) < lista.size() - 1) {
                //asignamos el siguiente valor al fichero
                diccionario = lista.get(lista.indexOf(f) + 1);
            }else{
                System.out.println("Diccionario desconocido, deberá introducir las palabras de forma manual.");
            }
        }else{
            System.out.println("Diccionario desconocido, deberá introducir las palabras de forma manual.");
        }
    }

    //este método añade una nueva palabra (junto a su traducción) a la lista
    public static void añadir(String original, String trad){
        traduccion.add(original + ";" + trad + "\n");
    }

    private static void guardar() {
        try {
            //creamos un nuevo fichero
            File f = new File("nuevoDiccionario.txt");
            BufferedWriter bwr = new BufferedWriter(new FileWriter(f));
            //guardamos en el fichero todas las traducciones
            for (String string : traduccion) {
                bwr.write(string);
            }
            bwr.close();
        //pilla los posibles errores que se pueden dar
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
}