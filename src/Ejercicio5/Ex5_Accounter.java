package Ejercicio5;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

//no se por que no va, lo ejecuto y me hace el calculo bien pero no me lo guarda en 'Totals.txt'
//pero lo vuelvo a ejecutar y me dice que el fichero no contiene números
public class Ex5_Accounter {

    public static void main(String[] args) {
        //comprobamos si los argumentos son mayor que 0
        if (args.length > 0) {

            List<String> list = Arrays.asList(args);
            //creamos el comando para la ejecución del hijo
            String command = "java -jar out/Ejercicio5_jar/MultiprocesoEdgar2.jar";
            //asignamos el nombre del fichero
            String fichero = "Totals.txt";
            ProcessBuilder pb;
            Scanner trac;

            Double totalGlobal = 0.0;

            List<String> com = new ArrayList<>(Arrays.asList(command.split(" ")));
            try {
                BufferedWriter bw = new BufferedWriter(new FileWriter(fichero));
                //creamos un bucle para añadir todos los números usados
                for (int i = 0; i < list.size(); i++) {
                    com.add(list.get(i));
                    pb = new ProcessBuilder(com);
                    Process process = pb.start();

                    trac = new Scanner(process.getInputStream());
                    String resultado = trac.nextLine();

                    Double total = Double.valueOf(resultado);
                    bw.write(list.get(i) + " -> " + total + "\n");
                    com.remove(list.get(i));
                }
                //aquí añade la suma total
                bw.write("-------------------\n");
                bw.write("Total: " + totalGlobal + "\n");
                bw.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        //da el anuncio si el archivo no cuenta con números
        }else{
            System.out.println("Debes introducir archivos con números...");
        }

    }
}
