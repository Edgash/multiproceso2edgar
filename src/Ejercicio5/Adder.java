package Ejercicio5;

import java.io.BufferedReader;
import java.io.FileReader;

public class Adder { public static void main(String[] args) {
    if (args.length > 0) {

    String fichero = args[0];
    //creamos un contador desde 0
    Double total = 0.0;
    try (BufferedReader br = new BufferedReader(new FileReader(fichero))) {
        //leemos el contenido del fichero
        String linea = br.readLine();
        while(linea != null) {
            //sumamos el el nuevo número al total
            total += Double.valueOf(linea);
            //leemos la siguiente línea
            linea = br.readLine();
        }
        //mostramos el total por pantalla
        System.out.println("El total ha sido: " + total);
        //si el contenido no es numerico lanzara un anuncio
    } catch(Exception e) {
        System.out.println(e.getMessage());
    }
    //si no se cumple lo anterior mostrará un mensaje
    } else {
        System.out.println("No se han pasado argumentos...");
}
}
}